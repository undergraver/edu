Book sites: http://www.dspguide.com
https://www.manpres.ro/Evrika+-+revista+de+fizica+-+Braila+2022-p5947
http://manualul.info
https://www.mateinfo.ro/evaluare-nationala-matematica/modele-de-teste-evaluare-nationala-modele-de-teste-pentru-evaluarea-nationala-la-matematica
http://www.matearena.ro
https://www.olimpiade.ro/
https://www.infoarena.ro

Inspiration: https://en.wikipedia.org/wiki/Bret_Victor

Collection of useful resources for education - started from manualul.info website

NOTE: Try shrinking large binary PDFs so that they don't occupy that much space.

In order to achieve this commands like the ones below can be used:
* gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook \
-dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf
* gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dDownsampleColorImages=true \
-dColorImageResolution=150 -dNOPAUSE  -dBATCH -sOutputFile=output.pdf input.pdf

and modify parameters like image resolution and PDFSETTINGS (/screen is for low quality for example).

More details can be found also here: https://askubuntu.com/questions/113544/how-can-i-reduce-the-file-size-of-a-scanned-pdf-file
